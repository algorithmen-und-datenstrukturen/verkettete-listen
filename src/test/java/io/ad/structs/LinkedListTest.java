package io.ad.structs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//TODO Complete Test class.
public class LinkedListTest {

    LinkedList<Integer> testList;

    @BeforeEach
    void setup() {
        testList = new LinkedList<>();
        testList.add(0);
        testList.add(1);
        testList.add(2);
    }

    @Test
    void testCreateEmptyList() {
        LinkedList<Integer> emptyList = new LinkedList<>();
        assertEquals(0, emptyList.size());
    }

    @Test
    void testSize() {
        int currentSize = testList.size();
        testList.add(3);
        assertEquals(currentSize+1, testList.size());
    }
    
}