package io.ad.structs;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<E> implements List<E> {
	private ListNode head;
	private ListNode last;
	private int size;

	// ListNode as member inner class.
	private class ListNode {
		private ListNode next;
		private E data;

		private ListNode(E data, ListNode next) {
			setData(data);
			setNext(next);
		}

		private void setData(E data) {
			this.data = data;
		}

		private void setNext(ListNode next) {
			this.next = next;
		}
	}

	@Override
	public boolean isEmpty() {
		return head == null;
	}

	@Override
	public void add(int index, E element) {
		if (element == null) {
			throw new NullPointerException("elem must not be null");
		}
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}
		if (index >= size() + 1) {
			throw new IndexOutOfBoundsException();
		}
		if (index == 0) {
			ListNode newHead = new ListNode(element, head);
			head = newHead;
			if (size()==0) last=head;
			size++;
			return;
		}
		if (index == size()) {
			last.setNext(new ListNode(element, null));
			last=last.next;	
			size++;
			return;
		}

		ListNode temp = head;
		for (int i = 0; i < index-1; i++) {
			temp = temp.next;
		}
		temp.setNext(new ListNode(element, temp.next));
		size++;
	}

	@Override
	public void add(E elem) {
		add(size, elem);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public E get(int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}		
		if (index >= size()) {
			throw new IndexOutOfBoundsException();
		}
		if (size() == 0) {
			throw new NoSuchElementException();
		}
		ListNode temp = head;
		for (int i = 0; i < index; i++) {
			temp = temp.next;
		}
		return temp.data;
	}

	public void addFirst(E elem) {
		add(0, elem);
	}

	public void addLast(E elem) {
		add(size,elem);
	}

	public E getFirst() {
		return get(0);
	}

	public E getLast() {
		return get(size() - 1);
	}

	public String toString() {
		ListNode temp = head;
		StringBuilder sb = new StringBuilder("[");
		while (temp != null) {
			if(sb.length()!=1) sb.append(", ");
			sb.append(temp.data);
			temp = temp.next;
		}
		sb.append("]");
		return sb.toString();
	}

	// Iterator implemented with anonymous inner class

	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private ListNode next = LinkedList.this.head;
	        public boolean hasNext() {
	           return next!=null;
	        }
	        public E next() {
	           if (!hasNext()) throw new NoSuchElementException();
	           E value = next.data;
	           next = next.next;
	           return value;
	        }
	     };
	}

	@Override
	public void remove(int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}		
		if (index >= size()) {
			throw new IndexOutOfBoundsException();
		}
		if (size() == 0) {
			throw new NoSuchElementException();
		}
		if (index==0) {
			head = head.next;
		} else {
			ListNode node = head;
			for (int pos = 0; pos<index-1; pos++) {
				node=node.next;
			}
			node.next = node.next.next;
			if (index==size()-1) last=node;
		}
		size--;
	}
}