package io.ad.structs;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;



public class DoubleLinkedList<E> implements List<E> {
	private ListNode head;
	private ListNode last;
	private int size;

	// ListNode as member inner class.
	private class ListNode {
		private ListNode next;
		private ListNode prev;
		private E data;

		private ListNode(E data, ListNode prev, ListNode next) {
			this.data = data;
			this.next = next;
			this.prev=prev;
		}
	}
	// ---------------------------------

	@Override
	public boolean isEmpty() {
		return head == null;
	}

	@Override
	public void add(int index, E element) {
		if (element == null) {
			throw new NullPointerException("elem must not be null");
		}
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}
		if (index >= size() + 1) {
			throw new IndexOutOfBoundsException();
		}
		if (index == 0) {
			ListNode newHead = new ListNode(element, null, head);
			if (head!=null)
				head.prev=newHead;
			head = newHead;
			if (size()==0) last=newHead;
			size++;
			return;
		}
		if (index == size()) {
			ListNode temp = new ListNode(element, last, null);
			last.next=temp;
			last=temp;
			size++;
			return;
		}

		ListNode temp = head;
		for (int i = 0; i < index - 1; i++) {
			temp = temp.next;
		}
		ListNode node = new ListNode(element, temp, temp.next);
		temp.next.prev=node;
		temp.next=node;
		size++;
	}

	@Override
	public void add(E elem) {
		if (head == null) {
			add(0, elem);

		} else {
			add(size, elem);
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public E get(int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}		
		if (index >= size()) {
			throw new IndexOutOfBoundsException();
		}
		if (size() == 0) {
			throw new NoSuchElementException();
		}
		if (index == size()-1) return last.data;
		else {
			ListNode temp = head;
			for (int i = 0; i < index; i++) {
				temp = temp.next;
			}
			return temp.data;
		}
	}

	public void addFirst(E elem) {
		add(0, elem);
	}

	public void addLast(E elem) {
		add(size,elem);
	}

	public E getFirst() {
		return get(0);
	}

	public E getLast() {
		return get(size() - 1);
	}

	public String toString() {
		ListNode temp = head;
		StringBuilder sb = new StringBuilder("[");
		while (temp != null) {
			if(sb.length()!=1) sb.append(", ");
			sb.append(temp.data);
			temp = temp.next;
		}
		sb.append("]");
		return sb.toString();
	}

	// Iterator as anonymous inner class

	public Iterator<E> iterator() {
		return new Iterator<E>() {
			ListNode next = DoubleLinkedList.this.head;
	        public boolean hasNext() {
	           return next!=null;
	        }
	        public E next() {
	           if (!hasNext()) throw new NoSuchElementException();
	           E value = next.data;
	           next = next.next;
	           return value;
	        }
	     };
	}
	
	// ListIterator as anonymous inner class
	public ListIterator<E> listIterator() {
		return new ListIterator<E>() {
			
			private ListNode next = head;
			private ListNode prev = null;
			private int nextIndex = 0;

			@Override
			public boolean hasNext() {
				return next!=null;
			}

			@Override
			public E next() {
				if (hasNext()) {
					nextIndex++;
					E data = next.data;
					next=next.next;
					if (prev==null) prev=head;
					else prev=prev.next;
					return data;
				} else {
					throw new NoSuchElementException();
				}
			}

			@Override
			public boolean hasPrevious() {				
				return prev!=null;
			}

			@Override
			public E previous() {
				if (hasPrevious()) {
					nextIndex--;
					E data = prev.data;
					prev=prev.prev;
					if (next==null) next=last;
					else next=next.prev;
					return data;
				} else {
					throw new NoSuchElementException();
				}
			}

			@Override
			public int nextIndex() {
				return nextIndex;
			}

			@Override
			public int previousIndex() {
				return nextIndex-1;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
				
			}

			@Override
			public void set(E e) {
				throw new UnsupportedOperationException();
				
			}

			@Override
			public void add(E e) {
				throw new UnsupportedOperationException();
				
			}
			
		};
		
	}

	@Override
	public void remove(int index) {
		if (index < 0) {
			throw new IndexOutOfBoundsException();
		}		
		if (index >= size()) {
			throw new IndexOutOfBoundsException();
		}
		if (size() == 0) {
			throw new NoSuchElementException();
		}
		if (index==0) {
			head.next.prev=null;
			head = head.next;
			if (size()==1) last = null;
		} else {
			ListNode node = head;
			for (int pos = 0; pos<index-1; pos++) {
				node=node.next;
			}
			node.next.next.prev=node;
			node.next = node.next.next;
			
		}
		size--;
	}
}