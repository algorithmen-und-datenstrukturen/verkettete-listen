package io.ad.structs;

public interface List<E> extends Iterable<E> {

	void add(E element);
	void add(int index, E element);
	int size();
	void remove(int index);
	E get(int index);
	boolean isEmpty();    
}
